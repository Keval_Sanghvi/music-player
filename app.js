const title = document.querySelector("#title");
const img = document.querySelector("#music-img");
const audio = document.querySelector("#audio");
const container = document.querySelector(".container");
const backward = document.querySelector("#backward");
const playpause = document.querySelector("#play-pause");
const forward = document.querySelector("#forward");
const volume = document.querySelector("#volume");
const loop = document.querySelector("#loop");
const progressBar = document.querySelector("#progress-bar");

playpause.addEventListener("click", start);
backward.addEventListener("click", prev);
forward.addEventListener("click", next);
volume.addEventListener("click", volumeControl);
loop.addEventListener("click", loopControl);
audio.addEventListener("timeupdate", updateProgress);
progressBar.addEventListener("click", setProgress);
audio.addEventListener("ended", next);

let songs = ["makhna", "iphone", "bolly"];

// index for song
let i = 1;

initialLoad(songs[i]);

function initialLoad(song) {
    title.innerText = song + ".mp3";
    audio.src = `music/${song}.mp3`;
    img.src = `images/${song}.jpg`;
}

function start(e) {
    const btn = document.querySelector("#play-pause i");
    if (btn.classList.contains("fa-play")) {
        btn.className = "fa fa-lg fa-pause";
        container.className = "container play";
        audio.play();
    } else {
        btn.className = "fa fa-lg fa-play";
        container.className = "container";
        audio.pause();
    }
}

function prev(e) {
    i--;
    if (i < 0) {
        i = songs.length - 1;
    }
    initialLoad(songs[i]);
    playpause.firstElementChild.className = "fa fa-lg fa-pause";
    audio.play();
    container.classList.add("play");
}

function next(e) {
    i++;
    if (i > songs.length - 1) {
        i = 0;
    }
    initialLoad(songs[i]);
    playpause.firstElementChild.className = "fa fa-lg fa-pause";
    audio.play();
    container.classList.add("play");
}

function updateProgress(e) {
    const {
        duration,
        currentTime
    } = e.target;
    if (duration) {
        let min = Math.floor(e.target.duration / 60);
        let sec = Math.floor(e.target.duration % 60);
        document.querySelector("#totalTime").innerHTML = min + ":" + sec;
    }
    let min = Math.floor(e.target.currentTime / 60);
    let sec = Math.floor(e.target.currentTime % 60);
    if (sec < 10) {
        sec = `0${sec}`;
    }
    document.querySelector("#currentTime").innerHTML = min + ":" + sec;
    const progressPercent = (currentTime / duration) * 100;
    progressBar.style.width = `${progressPercent}%`;
}

function setProgress(e) {
    const width = e.target.clientWidth;
    const clickX = e.offsetX;
    const duration = audio.duration;
    audio.currentTime = (clickX / width) * duration;
}

function volumeControl(e) {
    const vol = document.querySelector("#volume i");
    if (vol.classList.contains("fa-volume-up")) {
        vol.className = "fa fa-volume-down";
        audio.volume = 0.5;
    } else if (vol.classList.contains("fa-volume-down")) {
        vol.className = "fa fa-volume-off";
        audio.volume = 0;
    } else {
        vol.className = "fa fa-volume-up";
        audio.volume = 1;
    }
}

function loopControl() {
    const looping = document.querySelector("#loop i");
    if (looping.classList.contains("fa-random")) {
        looping.className = "fa fa-repeat";
        audio.loop = true;
    } else {
        looping.className = "fa fa-random";
        audio.loop = false;
    }
}

document.querySelector("#download").addEventListener("click", function () {
    let d = document.querySelector("#downl");
    d.setAttribute("href", "javascript:window.open(`music/${songs[i]}.mp3`)");
});
